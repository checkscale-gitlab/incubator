#!/bin/sh
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock:z \
    -v $PWD:/app:ro -v /tmp:/tmp:z --privileged=true python:$1 sh \
    -c "pip install pytest-cov && \
        cp -r /app /source && \
        cd /source && \
        $2 \
        python setup.py --requires > requires && \
        pip install -r requires && \
        py.test --cov=incubator tests/ "