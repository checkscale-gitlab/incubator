import os
import shutil
import tempfile
import unittest

from incubator.core.client import DockerClient
from incubator.core.utils import tar


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.client = DockerClient()

    def test_context_file_limit(self):
        def _make_temp_dir_context(file_size):
            build_dir = tempfile.mkdtemp()
            if file_size > 0:
                with open(os.path.join(build_dir, "file"), mode="wb") as file:
                    file.seek(file_size - 1)
                    file.write("1".encode())
            return build_dir

        dc = DockerClient()
        for size, limit in [(0, 0), (100, 0),
                            (100, 10239), (100, 10240), (100, 10241)]:
            dir = _make_temp_dir_context(size)
            context = tar(path=dir, client=dc, limit=limit)
            shutil.rmtree(dir)
            context.seek(0, 2)
            context_size = context.tell()
            is_in_memory = context.name is None
            self.assertEqual(is_in_memory, context_size <= limit or limit == 0)


if __name__ == '__main__':
    unittest.main()
