import logging
import unittest

import six

import incubator.api
from incubator.core.client import DockerClient


class TestImageAtributes(unittest.TestCase):
    def setUp(self):
        self.client = DockerClient()

    @classmethod
    def setUpClass(cls):
        (DockerClient()).pull_image("busybox")
        incubator.set_logging(level=logging.DEBUG)

    def test_env(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'ENV key=value another_key="another value"\n'
                                                        'ENV key3 val3\n'
                                                        'ENV key4=value4\n'
                                                        'ENV key4=different_value4\n'.encode()))

        config_info = self.client.get_image_info(image=image.id)["Config"]

        envs = config_info["Env"]
        self.assertIn("key=value", envs)
        self.assertIn("another_key=another value", envs)
        self.assertIn("key3=val3", envs)
        self.assertIn("key4=different_value4", envs)

    def test_label(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'LABEL key=value another_key="another value"\n'
                                                        'LABEL key3 val3\n'
                                                        'LABEL key4=value4\n'
                                                        'LABEL key4=different_value4\n'.encode()))

        config_info = self.client.get_image_info(image=image.id)["Config"]

        labels = config_info["Labels"]
        self.assertEqual(labels["key"], "value")
        self.assertEqual(labels["another_key"], "another value")
        self.assertEqual(labels["key3"], "val3")
        self.assertEqual(labels["key4"], "different_value4")

    def test_default_labels(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'LABEL key=value another_key="another value"\n'
                                                        'RUN touch /tmp/a\n'
                                                        'EXPOSE 80\n'
                                                        'ADD . /app/\n'.encode()))

        info = self.client.get_image_info(image=image.id)

        labels = info["Config"]["Labels"]
        self.assertEqual(labels["layer_0_commands"],
                         u' FROM busybox LABEL key=value another_key="another value" RUN touch /tmp/a')
        self.assertEqual(labels["layer_1_commands"], u' EXPOSE 80 ADD . /app/')

        self.assertEqual(info["Comment"], u'This layer is composed of these commands: EXPOSE 80 ADD . /app/')

    def test_workdir(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'WORKDIR /tmp'.encode()))

        info = self.client.get_image_info(image=image.id)

        config_info = info["Config"]
        self.assertEqual(config_info["WorkingDir"], "/tmp")

    def test_port(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'EXPOSE 80\n'
                                                        'EXPOSE 81 82'.encode()))

        info = self.client.get_image_info(image=image.id)

        config_info = info["Config"]
        self.assertEqual(config_info["ExposedPorts"], {"80": {}, "81": {}, "82": {}})

    def test_stop_signal(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'STOPSIGNAL SIGTERM'.encode()))

        info = self.client.get_image_info(image=image.id)

        config_info = info["Config"]
        self.assertEqual(config_info["StopSignal"], "SIGTERM")

    def test_user(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'USER 123456'.encode()))

        info = self.client.get_image_info(image=image.id)

        config_info = info["Config"]
        self.assertEqual(config_info["User"], "123456")

    def test_cmd(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'CMD --option argument'.encode()))

        info = self.client.get_image_info(image=image.id)

        config_info = info["Config"]
        self.assertEqual(config_info["Cmd"], ["--option argument"])

    def test_entrypoint(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'ENTRYPOINT ./app --option argument'.encode()))

        info = self.client.get_image_info(image=image.id)

        config_info = info["Config"]
        self.assertEqual(config_info["Entrypoint"], ["./app --option argument"])

    def test_volume(self):
        image = incubator.api.build(fileobj=six.BytesIO("FROM busybox\n"
                                                        'VOLUME /one/path\n'
                                                        'VOLUME /path/vol1 /path/vol2'.encode()))

        info = self.client.get_image_info(image=image.id)

        config_info = info["Config"]
        self.assertEqual(config_info["Volumes"], {"/one/path": {},
                                                  "/path/vol1": {},
                                                  "/path/vol2": {}})


if __name__ == '__main__':
    unittest.main()
