"""
Utility functions for tests.
"""
from __future__ import print_function, unicode_literals

import os
import shutil
import tarfile
import tempfile

import six

from incubator.api import build
from incubator.core.output import CHAPTER, SUBTITLE


def _print_all_lines(lines):
    if not isinstance(lines, list):
        output_lines = [lines]
    else:
        output_lines = list(lines)
    output_lines.append("")

    for l in output_lines:
        print(l)


def _output(subtitle, orig_value, incubator_value):
    print(SUBTITLE.format(subtitle))
    print(CHAPTER.format("original builder"))
    _print_all_lines(orig_value)
    print(CHAPTER.format("incubator builder"))
    _print_all_lines(incubator_value)


def create_image_orig(container_client, **kwargs):
    return container_client.build_image(**kwargs)


def create_image_builder(**kwargs):
    return build(**kwargs).id


def _format_output(text):
    output = []
    if not isinstance(text, list):
        text = [text]
    for t in text:
        if not isinstance(t, list):
            t = [t]
        for part in t:
            if not isinstance(part, six.string_types):
                part = part.decode()
            output += [l for l in part.split("\n")]
        if output[-1] == "":
            del (output[-1])
    return output


def get_result(image, command, client):
    output = []
    for l in client.run_container(image=image, command=command, remove=True):
        output += _format_output(l)
    return output


def make_test_on_containers(client, command, config=None, **kwargs):
    config = config or {}

    command = 'sh -c "{}" '.format(command)

    image_incubator = create_image_builder(container_client=client,
                                           config=config,
                                           **kwargs)

    image_orig = create_image_orig(container_client=client,
                                   **kwargs)
    _output("IMAGE", image_orig, image_incubator)

    result_builder = get_result(image_incubator, command, client)
    result_orig = get_result(image_orig, command, client)
    _output("RESULT", result_orig, result_builder)

    try:
        client.remove_image(image_incubator.id)
        client.remove_image(image_orig.id)
    except:
        pass

    return result_builder, result_orig


def make_test_on_containers_local(client, dockerfile_content, command,
                                  context_tempdir_function=None,
                                  build_directory=None):
    build_directory = build_directory or context_tempdir_function()

    if dockerfile_content:
        with open(os.path.join(build_directory, "Dockerfile"), mode="w") as dockerfile:
            dockerfile.writelines([dockerfile_content])

    image_incubator = build(path=build_directory).id
    image_orig = client.build_image(path=build_directory)

    shutil.rmtree(build_directory)

    _output("IMAGE", image_orig, image_incubator)

    result_incubator = _format_output(get_result(image_incubator, command, client))
    result_orig = _format_output(get_result(image_orig, command, client))

    _output("RESULT", result_orig, result_orig)

    return result_incubator, result_orig


def get_test_dir():
    build_dir = tempfile.mkdtemp()
    dirs = ["DA", "DB", "DC", "dir-a", "DA/D", "D F", "D", "D/AD", "B", "B/BB"]
    for d in dirs:
        os.makedirs(os.path.join(build_dir, d))
    for f in ["DA/a", "DA/D/e", "DA/D/c", "DA/D/x", "D/AD/y", "file.txt", "eee.txt"]:
        with open(os.path.join(build_dir, f), mode="w") as file:
            file.writelines(["FILE\n"])
    return build_dir


def get_test_dir_with_archive():
    build_dir = tempfile.mkdtemp()
    tar_dir = get_test_dir()
    with tarfile.open(os.path.join(build_dir, "archive.tar"), mode="w") as tar:
        tar.add(tar_dir)
    shutil.rmtree(tar_dir)
    return build_dir
