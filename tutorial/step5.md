One of the main taks of the ***Incubator*** is to be able to define the layer structure on your own. The splits between the instructions can be defined with the `-s` option(s). There are also two shortcuts for the two edge cases (`--squash`/`--default-layering`). The results you can control with the `docker history myimage`.

## Explicit layering

To set a layer split after instruction number *i*, set option `-s i`. You can set multiple of them.

`incubator build -t myimage -s 2 -s 3`

> The layers are not split always. When there are only *metadata-changes*, the split is not aplied.


## Squash

To have alll instruction in one layer, use `--squash` option:

`incubator build -t myimage --squash`

## Default layering

The default behaviour of the Docker builder (as well as for the *Incubator*) is to create a layer for each (*non-metadata-change*) layer. The layering can be defined also in the configuration file (more about it later). If you need to explicitelly set the default behaviour, you can use the `--default-layering` option.

`incubator build -t myimage --default-layering`
