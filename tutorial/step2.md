The easy way to use ***Incubator*** is with the command-line utility `incubator`.

Try running `incubator` command or use the provided help messages:

`incubator --help`

`incubator build --help`

> You can use `-h` instead of `--help`.

The `incubator build` is essential and nowadays the only one subcommand of the ***Incubator***.

You can also use the bash completion. It works for subcommand as well as for options. Just try typing the `incubator build -` and then press `TAB` twice.
